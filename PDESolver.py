import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button, Slider
import control as ct
import sympy as sm

print('Select slider view by typing any key or manual coefficient entry by pressing "enter". Manual coefficient enables printing of solution f(t)')
use_widget = bool(input())
max_order = 7

slider_dragging = True

t_vec = np.linspace(0,20,300)


def get_order(max_order):
    flag = False
    while not flag:
        print(f'enter order of differential equation: (positive value <= {max_order})')
        try:
            x = input()
            n = int(x)
            assert n > 0
            assert n <= max_order
            flag = True
        except (EOFError, KeyboardInterrupt):
            exit(1)
        except:
            print("\033[91m invalid input! \033[0m")

    return n

def solve(system, X0, u=0):
    if u:
        t,f = ct.forced_response(system, t_vec, u, X0)
    else:
        t, f = ct.initial_response(system, t_vec, X0)
    return t, f

def get_coefficients_from_terminal(n):
    flag = False
    while not flag:
        print(f'enter (comma separated) coeffients of the homogeneous differential equation f(n) = c0*f + c1*f(1) +...+ cn-1*f(n-1) of order n={n}') 
        try:
            Ab = [float(i) for i in input().split(",")]
            assert len(Ab) == n
            flag = True
        except (EOFError, KeyboardInterrupt):
            exit(1)
        except:
            print(f"\033[91m invalid input! You should provide {n} coefficients\033[0m")

    return compute_system_matrix(Ab)

def get_slider_values(sliders):
    return [slider.val for slider in sliders]



def get_system_from_coeff_list(coeffs):
    Ab = [float(i) for i in coeffs]
    return compute_system_matrix(Ab)


def compute_system_matrix(Ab):
    n = len(Ab)
    In = np.eye(n-1)
    Al = np.zeros((n-1, 1))
    At = np.block([[Al,In]])
    A = np.block([[At],[np.matrix(Ab)]])
    C = np.zeros((1,n))
    C[0][0] = 1
    B = np.zeros((n,1))
    B[-1][0] = 1
    system = ct.ss(A,B,C,0)
    return system

def get_initial_conditions_from_list(X0):
    return X0

def get_initial_conditions_from_terminal(n):
    flag = False
    while not flag:
        print(f'enter (comma separated) initial conditions for f + derivatives of order n={n}') 
        try:
            X0 = [float(i) for i in input().split(",")]
            assert len(X0) == n
            flag = True
        except (EOFError, KeyboardInterrupt):
            exit(1)
        except:
            print(f"\033[91m invalid input! You should provide {n} initial conditions \033[0m")

    return X0

def get_input_from_terminal():

    flag = False
    while not flag:
        print(f'enter external input u (0 for homogeneous):') 
        try:
            u = float(input())
            flag = True
        except (EOFError, KeyboardInterrupt):
            exit(1)
        except:
            print(f"\033[91m invalid input! You should provide a number \033[0m")

    return u



def create_widget(n):

    init_coeffs = [1]*n
    init_initial_condition = [1]*n
    init_u = 0
    system = get_system_from_coeff_list(init_coeffs)
    X0 = get_initial_conditions_from_list(init_initial_condition)
    u = init_u

    fig, ax = plt.subplots()
    ax.grid(True)
    t, f = solve(system, X0, u)
    line, = ax.plot(t, f, lw=2)
    ax.set_xlabel('Time [s]',fontsize=8)

    fig.subplots_adjust(left=0.25, bottom=0.30)

    ax_coeff_default = [0.37, 0.2, 0.45, 0.02]
    coeff_slider_list = []
    for i in range(n):
        ax_coeff = ax_coeff_default.copy()
        ax_coeff[1] -= i*0.03
        coeff_slider = Slider(
            ax=fig.add_axes(ax_coeff),
            label=f'{i}th coeff',
            valmin=-20,
            valmax=20,
            valstep = 0.2,
            valinit=init_coeffs[i],
            dragging = slider_dragging
        )
        coeff_slider_list.append(coeff_slider)

    fig.text(0.16-n*0.015, 0.85, 'init conditions')
    ax_cond_default = [0.19, 0.35, 0.02, 0.45]
    cond_slider_list = []
    for i in range(n):
        ax_cond = ax_cond_default.copy()
        ax_cond[0] -= i*0.03
        cond_slider = Slider(
            ax=fig.add_axes(ax_cond),
            label=f'{n-i-1}th',
            valmin=-20,
            valmax=20,
            valstep = 0.2,
            valinit=init_initial_condition[i],
            orientation="vertical",
            dragging = slider_dragging
        )
        cond_slider_list.append(cond_slider)

    ax_input = [0.93, 0.35, 0.02, 0.45]
    input_slider = Slider(
            ax=fig.add_axes(ax_input),
            label=f'u',
            valmin=-20,
            valmax=20,
            valstep = 0.2,
            valinit=init_u,
            orientation="vertical",
            dragging = slider_dragging
        )


    # The function to be called anytime a slider's value changes
    def update(val):
        
        coeffs = get_slider_values(coeff_slider_list)
        system = get_system_from_coeff_list(coeffs)
        init_conditions = get_slider_values(cond_slider_list)
        X0 = get_initial_conditions_from_list(init_conditions)
        u = input_slider.val

        t, f = solve(system, X0, u)
        line.set_ydata(f)

        ax.relim()
        ax.autoscale_view(True,True,True)
        


    # register the update function with each slider
    for slider in coeff_slider_list:
        slider.on_changed(update)
    for slider in cond_slider_list:
        slider.on_changed(update)
    input_slider.on_changed(update)

    return coeff_slider_list+cond_slider_list+[input_slider]







n = get_order(max_order)

if use_widget:
    sliders = create_widget(n)
    plt.grid()
    plt.show()
else:
    system = get_coefficients_from_terminal(n)
    X0 = get_initial_conditions_from_terminal(n)
    u = get_input_from_terminal()

    t, f = solve(system, X0, u)

    plt.plot(t, f)
    plt.xlabel("t")
    plt.ylabel("f(t)")
    plt.grid()
    

    t = sm.symbols('t')
    A = system.A
    At = sm.Matrix(A)*t
    eAt = sm.exp(At)
    f = eAt*sm.Matrix(X0)
    print('f(t) =', f[0])
    plt.show()


