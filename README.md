First, choose wether you want to manually input coefficients or use sliders to enter the parameters of the differential equation.
Simply pressing "enter" with no other inputs results in manual entry being chosen, while pressing any key before "enter" will result in slider mode being chosen.
The advantage of manual entry is that the function f(t) can be computed and displayed,
wheras with sliders there is much more freedom to make visual representations of the function.